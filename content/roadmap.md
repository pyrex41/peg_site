---
title: "Roadmap"
date: 2022-12-20T16:59:28-08:00
draft: 
--- 
* [x] Whitepaper -- first draft complete
* [ ] MVP smart contract code -- 70% complete
* [ ] Test net deployment
* [ ] MVP front-end development
* [ ] 3rd-party contract audit
* [ ] Production deployment
