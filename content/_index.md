# Overview
PEG is an over-collateralized, decentralized stablecoin built by tranching
collateral risk into two tokens, **PEG** and **POOL**.

**PEG** is like *debt*, **POOL** is like *equity* -- each have contingent claims on the underlying collateral. Risk cannot be engineered away, but it can be split apart and repackaged.

**PEG** is stable, and backed by excess collateral.

**POOL** is leveraged, but without liquidation risk.

Simple by design, **PEG** is a stablecoin framework that is transparent, robust, and extensible. 

For more, read the [whitepaper](pdf/paper.pdf) and checkout more technical materials [here](https://gitlab.com/pyrex41/pitchland).

# Questions?
Email: info@peglabs.io 
| Twitter: {{< newtabref href="https://twitter.com/reubbr" title="reubbr" >}}
 
 
